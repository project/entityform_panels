<?php
/**
 * @file
 * Task handler for the page manager.
 */

/**
 * Specialized implementation of hook_page_manager_task_tasks().
 *
 * See api-task.html for more information.
 */
function entityform_panels_entityform_edit_page_manager_tasks() {
  return array(
    // This is a 'page' task and will fall under the page admin UI.
    'task type' => 'page',

    'title' => t('Entityform add/edit form'),
    'admin title' => t('Entityform add/edit form'),
    'admin description' => t('When enabled, this overrides the default Drupal behavior for adding or edit entityforms at <em>entityform/%entityform/edit</em> and <em>eform/submit/%entityform_type</em>. If you add variants, you may use selection criteria such as entityform type or language or user access to provide different edit forms for entityforms. If no variant is selected, the default Drupal entityform edit will be used.'),
    'admin path' => 'entityform/%entityform/edit',

    // Menu hooks so that we can alter the entityform/%entityform menu entry to
    // point to us.
    'hook menu' => 'entityform_panels_entityform_edit_menu',
    'hook menu alter' => 'entityform_panels_entityform_edit_menu_alter',

    // This is task uses 'context' handlers and must implement these to give the
    // handler data it needs.
    'handler type' => 'context',
    'get arguments' => 'entityform_panels_entityform_edit_get_arguments',
    'get context placeholders' => 'entityform_panels_entityform_edit_get_contexts',

    // Allow this to be enabled or disabled:
    'disabled' => variable_get('entityform_panels_entityform_edit_disabled', TRUE),
    'enable callback' => 'entityform_panels_entityform_edit_enable',
    'access callback' => 'entityform_panels_entityform_edit_access_check',
  );
}

/**
 * Callback defined by entityform_panels_entityform_edit_page_manager_tasks().
 *
 * Alter the entityform edit input so that entityform edit comes to us rather
 * than the normal entityform edit process.
 */
function entityform_panels_entityform_edit_menu_alter(&$items, $task) {
  if (variable_get('entityform_panels_entityform_edit_disabled', TRUE)) {
    return;
  }
  $callback = $items['entityform/%entityform/edit']['page callback'];
  // Override the entityform edit handler for our purpose.
  if ($callback == 'entityform_form_wrapper' || variable_get('page_manager_override_anyway', FALSE)) {
    $items['entityform/%entityform/edit']['page callback'] = 'entityform_panels_entityform_edit';
    $items['entityform/%entityform/edit']['file path'] = $task['path'];
    $items['entityform/%entityform/edit']['file'] = $task['file'];
    $items['eform/submit/%entityform_empty']['page callback'] = 'entityform_panels_entityform_add';
    $items['eform/submit/%entityform_empty']['file path'] = $task['path'];
    $items['eform/submit/%entityform_empty']['file'] = $task['file'];
  }
  else {
    variable_set('entityform_panels_entityform_edit_disabled', TRUE);
    if (!empty($GLOBALS['entityform_panels_enabling_entityform_edit'])) {
      drupal_set_message(t('Page manager module is unable to enable entityform/%entityform/edit and eform/submit/% because some other module already has overridden with %callback.', array('%callback' => $callback)), 'warning');
    }
    return;
  }
}

/**
 * Entry point for our overridden entityform edit.
 *
 * This function asks its assigned handlers who, if anyone, would like
 * to run with it. If no one does, it passes through to
 * entityform edit, which is entityform_form_wrapper().
 */
function entityform_panels_entityform_edit($entityform) {
  // Load my task plugin.
  $task = page_manager_get_task('entityform_edit');

  // Load the entityform into a context.
  ctools_include('context');
  ctools_include('context-task-handler');

  $contexts = ctools_context_handler_get_task_contexts($task, '', array($entityform));

  $arg = array(isset($entityform->entityform_id) ? $entityform->entityform_id : $entityform->type);
  $output = ctools_context_handler_render($task, '', $contexts, $arg);
  if ($output === FALSE) {
    // Fall back!
    // We've already built the form with the context, so we can't build it
    // again, or form_clean_id will mess up our ids. But we don't really need
    // to, either:
    $context = reset($contexts);
    $output = $context->form;
  }
  return $output;
}

/**
 * Callback to handle the process of adding a entityform.
 *
 * This creates a basic $entityform and passes that off to
 * entityform_panels_entityform_edit().
 * It is modeled after Drupal's entityform_add() function.
 *
 * Unlike entityform_add() we do not need to check entityform_access because
 * that was already checked by the menu system.
 */
function entityform_panels_entityform_add($entityform) {
  // Load my task plugin.
  $task = page_manager_get_task('entityform_edit');

  // Load the entityform into a context.
  ctools_include('context');
  ctools_include('context-task-handler');

  $contexts = ctools_context_handler_get_task_contexts($task, '', array($entityform));

  $arg = array(isset($entityform->entityform_id) ? $entityform->entityform_id : $entityform->type);
  $output = ctools_context_handler_render($task, '', $contexts, $arg);
  if ($output === FALSE) {
    // Fall back!
    // We've already built the form with the context, so we can't build it
    // again, or form_clean_id will mess up our ids. But we don't really need
    // to, either:
    $context = reset($contexts);
    $output = $context->form;
  }
  return $output;
}

/**
 * Callback to get arguments provided by this task handler.
 *
 * Since this is the entityform edit and there is no UI on the arguments, we
 * create dummy arguments that contain the needed data.
 */
function entityform_panels_entityform_edit_get_arguments($task, $subtask_id) {
  return array(
    array(
      'identifier' => t('Entityform being edited'),
      'id' => 1,
      'name' => 'entityform_edit',
      'settings' => array(),
      'keyword' => 'entityform',
    ),
  );
}

/**
 * Callback to get context placeholders provided by this handler.
 */
function entityform_panels_entityform_edit_get_contexts($task, $subtask_id) {
  return ctools_context_get_placeholders_from_argument(entityform_panels_entityform_edit_get_arguments($task, $subtask_id));
}

/**
 * Callback to enable/disable the page from the UI.
 */
function entityform_panels_entityform_edit_enable($cache, $status) {
  variable_set('entityform_panels_entityform_edit_disabled', $status);
  // Set a global flag so that the menu routine knows it needs
  // to set a message if enabling cannot be done.
  if (!$status) {
    $GLOBALS['entityform_panels_enabling_entityform_edit'] = TRUE;
  }
}

/**
 * Callback to determine if a page is accessible.
 *
 * @param array $task
 *   The task plugin.
 * @param string $subtask_id
 *   The subtask id.
 * @param array $contexts
 *   The contexts loaded for the task.
 *
 * @return bool
 *   TRUE if the current user can access the page.
 */
function entityform_panels_entityform_edit_access_check($task, $subtask_id, $contexts) {
  $context = reset($contexts);
  return entityform_access('update', $context->data);
}
