<?php
/**
 * @file
 * Plugin to provide an argument handler for en entityform edit form.
 */

/**
 * Creating the $plugin array.
 */
$plugin = array(
  'title' => t("Entityform edit form: entityform ID"),
  // Keyword to use for %substitution.
  'keyword' => 'entityform',
  'description' => t('Creates an entityform edit form context from an entityform ID argument.'),
  'context' => 'ctools_entityform_edit_context',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the entityform ID of an entityform for this argument'),
  ),
);

/**
 * Discover if this argument gives us the entityform we crave.
 */
function ctools_entityform_edit_context($arg = NULL, $conf = NULL, $empty = FALSE) {
  // If unset it wants a generic, unfilled context.
  if ($empty) {
    return ctools_context_create_empty('entityform_edit_form');
  }

  // We can accept either an entityform object or a pure nid.
  if (is_object($arg)) {
    return ctools_context_create('entityform_edit_form', $arg);
  }

  if (!is_numeric($arg)) {
    return FALSE;
  }

  $entityform = entityform_load($arg);
  if (!$entityform) {
    return NULL;
  }

  // This will perform an entityform_access check, so we don't have to.
  return ctools_context_create('entityform_edit_form', $entityform);
}
