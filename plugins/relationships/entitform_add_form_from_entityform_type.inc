<?php
/**
 * @file
 * Provides an relationship for entityform_add_form from entityform_type.
 */

$plugin = array(
  'title' => t('Entityform form from Entityform Type'),
  'keyword' => 'entityform_add_form',
  'description' => t('Adds entityform add from from an entityform type context.'),
  'required context' => new ctools_context_required(t('Entityform Type'), 'entity:entityform_type'),
  'context' => 'ctools_entityform_add_form_from_entityform_type_context',
);

/**
 * Return a new context based on an existing context.
 */
function ctools_entityform_add_form_from_entityform_type_context($context, $conf) {
  if (empty($context->data)) {
    return ctools_context_create_empty('entityform_add_form');
  }
  if (isset($context->data->type)) {
    return $context_cache[$context->data->type] = ctools_context_create('entityform_add_form', $context->data);
  }
}
