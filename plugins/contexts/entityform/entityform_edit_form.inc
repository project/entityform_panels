<?php
/**
 * @file
 * Plugin to provide a entityform_edit_form context.
 */

$plugin = array(
  'title' => t('Entityform edit form'),
  'description' => t('A entityform edit form.'),
  'context' => 'entityform_context_create_entityform_edit_form',
  'edit form' => 'entityform_context_entityform_edit_form_settings_form',
  'defaults' => array('type' => ''),
  'keyword' => 'entityform_edit',
  'context name' => 'entityform_edit_form',
  'convert list' => array('type' => t('Entityform type')),
  'convert' => 'entityform_context_entityform_edit_form_convert',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the entityform type of this context.'),
  ),
);

/**
 * Edit form for the task.
 *
 * It's important to remember that $conf is optional here, because contexts
 * are not always created from the UI.
 */
function entityform_context_create_entityform_edit_form($empty, $entityform = NULL, $conf = FALSE) {
  static $creating = FALSE;
  $form_cache = &drupal_static(__FUNCTION__, array());

  $context = new ctools_context(array(
    'form',
    'entityform_edit_form',
    'entity:entityform',
  ));
  $context->keyword = 'entityform_edit';
  $context->plugin = 'entityform_edit';

  if ($empty || ($creating)) {
    return $context;
  }
  $creating = TRUE;

  if (!empty($entityform)) {
    $type = $entityform->type;

    // Validate that the entityform type exists or user have access to edit.
    if ((!empty($entityform->is_new) && entityform_access('submit', $entityform->type))
      || entityform_access('edit', $entityform)) {

      // Create the form ONCE for this context. Creating the same form multiple
      // times screw's up the value handling as form_state becomes stall.
      if (!isset($form_cache[$type])) {
        // Do what entityform_form_wrapper() does.
        $form_cache[$type]['form_state'] = array(
          'want form' => TRUE,
          'build_info' => array(
            'args' => array($entityform, 'submit'),
          ),
        );
        // Use form_load_include  to ensure the form handler knows about it.
        form_load_include($form_cache[$type]['form_state'], 'inc', 'entityform', 'entityform.admin');
        $form_cache[$type]['form_id'] = $entityform->type . '_entityform_edit_form';
        $form_cache[$type]['form'] = drupal_build_form($form_cache[$type]['form_id'], $form_cache[$type]['form_state']);
        // Add global entityform classes.
        $form_cache[$type]['form']['#attributes']['class'][] = 'entityform';
        $form_cache[$type]['form']['#attributes']['class'][] = 'entitytype-' . $type . '-form';
        $form_cache[$type]['form']['#attributes']['class'][] = 'entity-entityform-edit';
      }
      $types = entityform_get_types();

      // In a form, $entityform is the object being edited.
      $context->data = $entityform;
      $context->title = (method_exists($types[$type], 'getTranslation')) ? $types[$type]->getTranslation('label') : $types[$type]->label;
      $context->argument = $type;

      // These are specific pieces of data to this form.
      // All forms should place the form here.
      $context->form = $form_cache[$type]['form'];
      $context->form_state = &$form_cache[$type]['form_state'];
      $context->form_id = $form_cache[$type]['form_id'];
      $context->form_title = (method_exists($types[$type], 'getTranslation')) ? $types[$type]->getTranslation('label') : $types[$type]->label;
      $context->entityform_type  = $type;
      $context->restrictions['type'] = array($type);
      $context->restrictions['form'] = array('form');

      $creating = FALSE;
      return $context;
    }
  }
  $creating = FALSE;
}

/**
 * The settings form.
 */
function entityform_context_entityform_edit_form_settings_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['entityform'] = array(
    '#title' => t('Entiyform'),
    '#type' => 'textfield',
    '#maxlength' => 512,
    '#autocomplete_path' => 'ctools/autocomplete/entityform',
    '#weight' => -10,
  );

  return $form;
}

/**
 * Submit handler for the settings form.
 */
function entityform_context_entityform_edit_form_settings_form_submit($form, &$form_state) {
  $form_state['conf']['entityform'] = $form_state['values']['entityform'];
}

/**
 * Convert a context into a string.
 */
function entityform_context_entityform_edit_form_convert($context, $type) {
  switch ($type) {
    case 'type':
      return $context->data->type;
  }
}
