<?php
/**
 * @file
 * Plugin to provide a entityform_add_form context.
 */

$plugin = array(
  'title' => t('Entityform add form'),
  'description' => t('A entityform add form.'),
  'context' => 'entityform_context_create_entityform_add_form',
  'edit form' => 'entityform_context_entityform_add_form_settings_form',
  'defaults' => array('type' => ''),
  'keyword' => 'entityform_add',
  'context name' => 'entityform_add_form',
  'convert list' => array('type' => t('Entityform type')),
  'convert' => 'entityform_context_entityform_add_form_convert',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the entityform type of this context.'),
  ),
);

/**
 * Form callback.
 *
 * It's important to remember that $conf is optional here, because contexts
 * are not always created from the UI.
 */
function entityform_context_create_entityform_add_form($empty, $data = NULL, $conf = FALSE) {
  static $creating = FALSE;
  $form_cache = &drupal_static(__FUNCTION__, array());

  $context = new ctools_context(array(
    'form',
    'entityform_add_form',
    'entity:entityform',
  ));
  $context->keyword = 'entityform_add';
  $context->plugin = 'entityform_add';

  if ($empty || ($creating)) {
    return $context;
  }
  $creating = TRUE;

  if (!empty($data)) {
    if ($data instanceof EntityformType) {
      $type = $data->type;
    }
    elseif (isset($data['type'])) {
      $type = $data['type'];
    }

    // Validate the entityform type exists and is accessible.
    if (entityform_access('create', $type)) {
      // Create the form ONCE for this context. Creating the same form multiple
      // times screw's up the value handling as form_state becomes stall.
      if (!isset($form_cache[$type])) {
        // Do what entityform_form_wrapper() does.
        $form_cache[$type]['entityform'] = entityform_empty_load($type);
        $form_cache[$type]['form_state'] = array(
          'want form' => TRUE,
          'build_info' => array(
            'args' => array($form_cache[$type]['entityform'], 'submit'),
          ),
        );
        // Use form_load_include  to ensure the form handler knows about it.
        form_load_include($form_cache[$type]['form_state'], 'inc', 'entityform', 'entityform.admin');
        $form_cache[$type]['form_id'] = $form_cache[$type]['entityform']->type . '_entityform_edit_form';
        $form_cache[$type]['form'] = drupal_build_form($form_cache[$type]['form_id'], $form_cache[$type]['form_state']);
        // Add global entityform class.
        $form_cache[$type]['form']['#attributes']['class'][] = 'entityform';
        $form_cache[$type]['form']['#attributes']['class'][] = 'entitytype-' . $type . '-form';
        $form_cache[$type]['form']['#attributes']['class'][] = 'entity-entityform-add';
      }

      // In a form, $data is the object being edited.
      $types = entityform_get_types();
      $context->data = $form_cache[$type]['entityform'];
      $context->title = (method_exists($types[$type], 'getTranslation')) ? $types[$type]->getTranslation('label') : $types[$type]->label;
      $context->argument = $type;

      // These are specific pieces of data to this form.
      // All forms should place the form here.
      $context->form = $form_cache[$type]['form'];
      $context->form_state = &$form_cache[$type]['form_state'];
      $context->form_id = $form_cache[$type]['form_id'];
      $context->form_title = (method_exists($types[$type], 'getTranslation')) ? $types[$type]->getTranslation('label') : $types[$type]->label;
      $context->entityform_type = $type;
      $context->restrictions['type'] = array($type);
      $context->restrictions['form'] = array('form');

      $creating = FALSE;
      return $context;
    }
  }
  $creating = FALSE;
}

/**
 * Edit form.
 */
function entityform_context_entityform_add_form_settings_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $options = array_keys(entityform_get_types());
  $options = array_combine($options, $options);

  $form['type'] = array(
    '#title' => t('Entiyform type'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $conf['type'],
    '#description' => t('Select the entityform type for this form.'),
  );

  return $form;
}

/**
 * Edit form submit.
 */
function entityform_context_entityform_add_form_settings_form_submit($form, &$form_state) {
  $form_state['conf']['type'] = $form_state['values']['type'];
}

/**
 * Convert a context into a string.
 */
function entityform_context_entityform_add_form_convert($context, $type) {
  switch ($type) {
    case 'type':
      return $context->data->type;
  }
}
